DESTDIR := /
PREFIX  := $(DESTDIR)usr
INSTALL := install

install:
	$(INSTALL) -m 0644 -D files/hlwm-gnome-flashback-session.desktop $(PREFIX)/share/xsessions/hlwm-gnome-flashback-session.desktop
	$(INSTALL) -m 0644 -D files/hlwm-gnome-flashback.desktop $(PREFIX)/share/applications/hlwm-gnome-flashback.desktop
	$(INSTALL) -m 0644 -D files/hlwm-gnome-flashback.session $(PREFIX)/share/gnome-session/sessions/hlwm-gnome-flashback.session
	$(INSTALL) -m 0755 -D files/hlwm-gnome-flashback-session $(PREFIX)/bin/hlwm-gnome-flashback-session
	$(INSTALL) -m 0755 -D files/hlwm-gnome-flashback $(PREFIX)/bin/hlwm-gnome-flashback
	$(INSTALL) -m 0644 -D files/hlwm-gnome-flashback.gschema.override $(PREFIX)/share/glib-2.0/schemas/01_hlwm-gnome-flashback.gschema.override
	glib-compile-schemas $(PREFIX)/share/glib-2.0/schemas/

uninstall:
	rm -f $(PREFIX)/bin/hlwm-gnome-flashback \
	      $(PREFIX)/bin/hlwm-gnome-flashback-session \
	      $(PREFIX)/share/gnome-session/sessions/hlwm-gnome-flashback.session \
	      $(PREFIX)/share/applications/hlwm-gnome-flashback.desktop \
	      $(PREFIX)/share/xsessions/hlwm-gnome-flashback-session.desktop \
	      $(PREFIX)/share/glib-2.0/schemas/01_hlwm-gnome-flashback.gschema.override
	glib-compile-schemas $(PREFIX)/share/glib-2.0/schemas/

.PHONY: install uninstall
